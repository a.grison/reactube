import React, {useState} from 'react';
import {Container} from 'react-bootstrap';
import MyNav from './components/MyNav';
import Video from "./components/Video";
import DetailedVideo from "./components/DetailedVideo";
import './App.css';

// router
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";

const App = () => {
    const [videos, setVideos] = useState([]);
    const [selectedVideo, selectVideo] = useState({});

    return (
        <Container className="p-3">
            <Router>
                <MyNav onResults={setVideos}/>

                <Switch>
                    <Route path="/videos/:videoId">
                        <DetailedVideo id={selectedVideo.id}
                                       snippet={selectedVideo.snippet} player={selectedVideo.player}
                                       statistics={selectedVideo.statistics} />
                    </Route>
                    <Route path="/search/:search">
                        <>
                            {videos.map(v => {
                                const {
                                    title, description, thumbnails,
                                    channelTitle, publishTime
                                } = v.snippet;

                                return (<Video
                                    videoId={v.id.videoId}
                                    thumbnail={thumbnails.high}
                                    description={description}
                                    channelTitle={channelTitle}
                                    publishTime={publishTime}
                                    title={title}
                                    selectVideo={selectVideo}/>);
                            })}
                        </>
                    </Route>
                    <Route path="/">
                        Merci d'effectuer une recherche...
                    </Route>
                </Switch>
            </Router>
        </Container>
    );
};

export default App;
